package com.example.appengine.java8;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.googlecode.objectify.Key;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Created by Gil on 23/08/2018.
 */

@Path("tracks")
public class APITest {

    @GET
    @Path("/add_track/{title}/{singer}")
    @Produces(MediaType.APPLICATION_JSON)
    public Track addTrack(@PathParam("title") String title, @PathParam("singer") String singer) {

        Track track = new Track();
        track.setTitle(title);
        track.setSinger(singer);

        ofy().save().entity(track).now();


        return track;

    }

    @GET
    @Path("get_track/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Track getTrackInJSONById(@PathParam("id") String stringId) {

        long id = Long.parseLong(stringId);


        Track track = ofy().cache(false).load().key(Key.create(Track.class, id)).now();

        if(track!=null) {
            return track;
        }else{
            return new Track("nulltrack", "nulltrack");
        }

    }

    @GET
    @Produces("text/plain")
    @Path("/hello")
    public String hellowWorld() {
        return "Hello World!";
    }


    @POST
    @Path("/post")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTrackInJSON(Track track) {

        String result = "Track saved : " + track;
        return Response.status(201).entity(result).build();

    }
}
