package com.example.appengine.java8;

import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.logging.Logger;

@WebListener
public class MyAppServletContextListener implements ServletContextListener {

    private static final Logger log = Logger.getLogger(MyAppServletContextListener.class.getName());
    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        log.info("ServletContextListener destroyed");
    }

    //Run this before web application is started
    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        log.info("ServletContextListener started");
        ObjectifyService.init();
        ObjectifyService.register(Track.class);
    }
}
