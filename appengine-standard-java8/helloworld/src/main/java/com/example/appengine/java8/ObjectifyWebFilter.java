package com.example.appengine.java8;

/**
 * Created by Gil on 22/08/2018.
 */

import com.googlecode.objectify.ObjectifyFilter;

        import javax.servlet.annotation.WebFilter;

@WebFilter(urlPatterns = {"/*"})
public class ObjectifyWebFilter extends ObjectifyFilter {}