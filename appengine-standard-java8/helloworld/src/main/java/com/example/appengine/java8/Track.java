package com.example.appengine.java8;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Created by Gil on 23/08/2018.
 */

@Entity
public class Track {
    @Id
    Long id;
    @Index
    String title;
    String singer;

    public Track() {}

    public Track(String title, String singer) {
        this.title = title;
        this.singer= singer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    @Override
    public String toString() {
        return "Track [title=" + title + ", singer=" + singer + ", id=" + id + "]";
    }

}
